#
# Copyright (C) 2025 The Android Open Source Project
# Copyright (C) 2025 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from ctwo device
$(call inherit-product, device/motorola/ctwo/device.mk)

PRODUCT_DEVICE := ctwo
PRODUCT_NAME := omni_ctwo
PRODUCT_BRAND := motorola
PRODUCT_MODEL := motorola edge 50 ultra
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="ctwo-user 14 UUVS34HV-V1-ST1.1 c2e335 release-keys"

BUILD_FINGERPRINT := motorola/ctwo/ctwo:14/UUVS34HV-V1-ST1.1/c2e335:user/release-keys
